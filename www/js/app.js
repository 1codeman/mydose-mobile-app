(function() {

var app = angular.module('mydoses', ['ionic', 'ngCordova', 'mydose.dosestore','mydose.services']);

app.config(function($stateProvider, $urlRouterProvider) {

   $stateProvider.state('home', {
    url: '/home',
    templateUrl: 'templates/home.html'
  });

  $stateProvider.state('list', {
    url: '/list',
    templateUrl: 'templates/list.html'
  });

  $stateProvider.state('myfavorites', {
    url: '/myfavorites',
    templateUrl: 'templates/myfavorites.html'
  });

  $stateProvider.state('show', {
    url: '/show/:doseId',
    templateUrl: 'templates/show.html',
    controller: 'EditCtrl'
  });

  $stateProvider.state('add', {
    url: '/add',
    templateUrl: 'templates/edit.html',
    controller: 'AddCtrl'
  });

  $stateProvider.state('edit', {
    url: '/edit/:doseId',
    templateUrl: 'templates/edit.html',
    controller: 'EditCtrl'
  });



  $urlRouterProvider.otherwise('/home');
});


app.controller('CameraCtrl', function($scope, $cordovaCamera) {

  // $scope.pictureUrl = 'http://placehold.it/300x300';

  // $scope.takePicture = function() {
  //   var options = {
  //     quality: 60,
  //     destinationType: Camera.DestinationType.DATA_URL,
  //     allowEdit: true,
  //     correctOrientatin: true,
  //     encodingType: Camera.EncodingType.JPEG,
  //     sourceType: Camera.PictureSourceType.CAMERA,
  //     targetWidth: 3000
  //   }
  //   $cordovaCamera.getPicture(options)
  //     .then(function(data) {
  //       //console.log('camera data: ' + angular.toJson(data));
  //       $scope.pictureUrl = 'data:image/jpeg;base64,' + data;
  //     }, function(error) {
  //       console.log('camera error: ' + angular.toJson(error));
  //     });
  // };

});


app.controller('ListCtrl', function($scope, $state, DoseStore) {


  
  $scope.reordering = false;
  $scope.doses = DoseStore.list();

  $scope.remove = function(doseId) {
    DoseStore.remove(doseId);
  };

  $scope.move = function(dose, fromIndex, toIndex) {
    DoseStore.move(dose, fromIndex, toIndex);
  };

  $scope.toggleReordering = function() {
    $scope.reordering = !$scope.reordering;
  };

    $scope.setFalseFavorite = function(doseId) {
    $scope.dose = angular.copy(DoseStore.get(doseId));
    $scope.dose.isFavorite = false;
    DoseStore.update($scope.dose);
    $state.go('myfavorites');
  };

});

app.controller('AddCtrl', function($scope, $state, DoseStore,$cordovaCamera) {
  $scope.lastPhoto="img/highdose.png"
  $scope.isShow = false;
  $scope.header = "Create Dose"; 
  $scope.dose = {
    id: new Date().getTime().toString(),
    title: '',
    description: '',
    ingredients: '',
    procedures: '',
    dosetype:'',
    img: '',
    isFavorite: false
  };

   $scope.isFavorite = function() {
    return true;
  };

  $scope.save = function() {
    $scope.dose.img =  $scope.pictureUrl 
    DoseStore.create($scope.dose);
    $state.go('list');
  };  

  $scope.pictureUrl = 'img/placeholder.png';

  $scope.takePicture = function() {
    var options = {
      quality: 60,
      destinationType: Camera.DestinationType.DATA_URL,
      allowEdit: true,
      correctOrientatin: true,
      encodingType: Camera.EncodingType.JPEG,
      sourceType: Camera.PictureSourceType.CAMERA,
      targetWidth: 3000
    }

    $cordovaCamera.getPicture(options)
      .then(function(data) {
        //console.log('camera data: ' + angular.toJson(data));
        $scope.pictureUrl = 'data:image/jpeg;base64,' + data;
      }, function(error) {
        console.log('camera error: ' + angular.toJson(error));
      });
  };  

   $scope.browsePicture = function() {
    var options = {
      quality: 60,
      destinationType: Camera.DestinationType.DATA_URL,
      allowEdit: true,
      correctOrientatin: true,
      encodingType: Camera.EncodingType.JPEG,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 3000
    }

    $cordovaCamera.getPicture(options)
      .then(function(data) {
        //console.log('camera data: ' + angular.toJson(data));
        $scope.pictureUrl = 'data:image/jpeg;base64,' + data;
      }, function(error) {
        console.log('camera error: ' + angular.toJson(error));
      });
  };  



  // $scope.getPhoto = function() {
  //   Camera.getPicture().then(function(imageURI) {
  //     console.log(imageURI);
  //     $scope.lastPhoto = imageURI;
  //   }, function(err) {
  //     console.err(err);
  //   }, {
  //      quality: 100,
  //     saveToPhotoAlbum: false,
  //     correctOrientation: true
  //   });
  // };

});

app.controller('EditCtrl', function($scope, $state, DoseStore, $cordovaCamera) {


  $scope.isShow = true;
  $scope.header = "Edit Dose";
  $scope.dose = angular.copy(DoseStore.get($state.params.doseId));
  $scope.pictureUrl = $scope.dose.img;

  $scope.save = function() {
    $scope.dose.img = $scope.pictureUrl;
    DoseStore.update($scope.dose);
    $state.go('list');
  };


  $scope.toggleFavorite = function() {
    $scope.dose.isFavorite = !$scope.dose.isFavorite;
    DoseStore.update($scope.dose);
    $state.go('myfavorites');
  };

   $scope.isFavorite = function() {
    return $scope.dose.isFavorite;
  };

    $scope.takePicture = function() {
    var options = {
      quality: 60,
      destinationType: Camera.DestinationType.DATA_URL,
      allowEdit: true,
      correctOrientatin: true,
      encodingType: Camera.EncodingType.JPEG,
      sourceType: Camera.PictureSourceType.CAMERA,
      targetWidth: 3000
    }
    
    $cordovaCamera.getPicture(options)
      .then(function(data) {
        //console.log('camera data: ' + angular.toJson(data));
        $scope.pictureUrl = 'data:image/jpeg;base64,' + data;
      }, function(error) {
        console.log('camera error: ' + angular.toJson(error));
      });
  };  


   $scope.browsePicture = function() {
    var options = {
      quality: 60,
      destinationType: Camera.DestinationType.DATA_URL,
      allowEdit: true,
      correctOrientatin: true,
      encodingType: Camera.EncodingType.JPEG,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 3000
    }

    $cordovaCamera.getPicture(options)
      .then(function(data) {
        //console.log('camera data: ' + angular.toJson(data));
        $scope.pictureUrl = 'data:image/jpeg;base64,' + data;
      }, function(error) {
        console.log('camera error: ' + angular.toJson(error));
      });
  };  


});



app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

}());
angular.module('mydose.dosestore', [])
	.factory('DoseStore', function() {

	  var doses = angular.fromJson(window.localStorage['doses'] || '[]');

	  function persist() {
	  	window.localStorage['doses'] = angular.toJson(doses);
	  }

	  return {

	    list: function() {
	      return doses;
	    },

	    get: function(doseId) {
	      for (var i = 0; i < doses.length; i++) {
	        if (doses[i].id === doseId) {
	          return doses[i];
	        }
	      }
	      return undefined;
	    },

	    create: function(dose) {

	      doses.push(dose);
	      persist();
	    },

	    setFavorite: function(dose) {
	      for (var i = 0; i < doses.length; i++) {
	        if (doses[i].id === dose.id) {
	          doses[i].isFavorite = dose.isFavorite;
	          persist();
	          return;
	        }
	      }
	    },
	    update: function(dose) {
	    	
		 
	      for (var i = 0; i < doses.length; i++) {
	        if (doses[i].id === dose.id) {
	          doses[i] = dose;
	          persist();
	          return;
	        }
	      }
	    },

	    move: function(dose, fromIndex, toIndex) {
	    	doses.splice(fromIndex, 1);
	    	doses.splice(toIndex, 0, dose);
	    	persist();
	    },

	    remove: function(doseId) {
	      for (var i = 0; i < doses.length; i++) {
	        if (doses[i].id === doseId) {
	          doses.splice(i, 1);
	          persist();
	          return;
	        }
	      }
	    }

	  };

	});
